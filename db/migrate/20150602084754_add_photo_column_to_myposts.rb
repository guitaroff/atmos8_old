class AddPhotoColumnToMyposts < ActiveRecord::Migration
  def change
    add_column :myposts, :photo, :string
  end
end
