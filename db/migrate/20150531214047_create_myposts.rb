class CreateMyposts < ActiveRecord::Migration
  def change
    create_table :myposts do |t|
      t.text :content
      t.integer :user_id

      t.timestamps null: false
    end
    add_index :myposts, [:user_id, :created_at]
  end
end
