class User < ActiveRecord::Base
  authenticates_with_sorcery!

  mount_uploader :photo, PhotoUploader

  has_many :myposts, dependent: :destroy

  has_many :relations, foreign_key: 'follower_id', dependent: :destroy
  has_many :followed_users, through: :relations, source: :followed
  has_many :reverse_relations, foreign_key: 'followed_id', class_name: 'Relation', dependent: :destroy
  has_many :followers, through: :reverse_relations, source: :follower

  validates :password, length: { minimum: 5 }, :on => :create
  validates :password, confirmation: true
  validates :email, uniqueness: true, email_format: { message: 'has invalid format' }

  def following?(user)
    relations.find_by(followed_id: user.id)
  end

  def follow!(user) #подписаться на юзера
    relations.create!(followed_id: user.id)
  end

  def unfollow!(user) #отписаться от юзера
    relations.find_by(followed_id: user.id).destroy!
  end

  def news_feed
    Mypost.followed_by(self)
  end
end
