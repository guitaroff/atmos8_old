class Mypost < ActiveRecord::Base

  mount_uploader :photo, PhotoUploader

  belongs_to :user
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 1000 }

  default_scope -> { order('created_at DESC') }

  def self.followed_by(user)
    joins('INNER JOIN relations ON myposts.user_id=relations.followed_id').where('relations.follower_id=:user_id OR myposts.user_id=:user_id', user_id: user.id).uniq
  end
end
