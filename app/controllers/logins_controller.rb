class LoginsController < ApplicationController
  def new
  end

  def create
    if @user = login(params[:email], params[:password], params[:remember_me])
      redirect_back_or_to(user_path(@user), notice: 'Ваша персональная страница!')
    else
      flash.now[:alert] = 'Войти не удалось'
      render action: 'new'
    end
  end

  def destroy
    logout
    redirect_to root_url, :notice => 'Ждём Вас снова!'
  end
end
