class UsersController < ApplicationController
  before_action :require_login, except: [:new, :create]
  before_action :current,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy
  before_action :find_user, only: [:show, :edit, :update, :destroy, :following, :followers]

  def index
    @users = User.page(params[:page]).per(5)
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      redirect_to root_url, :notice => 'Благодарим за регистрацию!'
    else
      render :new
    end
  end

  def show
    @myposts = @user.news_feed.page(params[:page]).per(5)
    @mypost = @user.myposts.new
  end

  def edit
  end

  def update
    if @user.update_attributes(user_params)
      redirect_to user_path(@user)
    else
      render 'edit'
    end
  end

  def destroy
        if @user == current_user
          flash[:notice] = 'Себя нельзя удалить!'
          redirect_to users_url
        else
          @user.destroy
          flash[:success] = 'User deleted.'
          redirect_to users_url
        end
  end

  def following
    @title = 'Following'
    @users = @user.followed_users.page(params[:page]).per(5)
    render 'show_follow'
  end

  def followers
    @title = 'Followers'
    @users = @user.followers.page(params[:page]).per(5)
    render 'show_follow'
  end

  private

  def user_params
    params.require(:user).permit(:username,:email, :password, :password_confirmation, :photo)
  end

  def find_user
    @user = User.find(params[:id])
  end

  def current
    @user = User.find(params[:id])
    redirect_to(root_url) if @user != current_user
  end

  def admin_user
    redirect_to(root_url) unless current_user.admin?
  end
end
