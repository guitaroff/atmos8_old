class MypostsController < ApplicationController
  before_action :require_login

  def create
    @mypost = current_user.myposts.new(mypost_params)
    if @mypost.save
      PostMailer.new_mypost(@mypost).deliver_later
      redirect_back_or_to(user_path(current_user), notice: 'Новая запись добавлена!')
    else
      flash[:alert] = 'Нет содержания!'
      redirect_to user_path(current_user)
    end
  end

  def destroy
    if @mypost = current_user.myposts.find_by(id: params[:id])
       @mypost.destroy
    end
    redirect_to user_path(current_user)
  end

  private

  def mypost_params
    params.require(:mypost).permit(:content, :photo)
  end
end
