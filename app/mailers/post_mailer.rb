class PostMailer < ApplicationMailer

  def new_mypost(mypost)
    @mypost = mypost
    @url  = 'http://0.0.0.0:3000/users/#{mypost.user_id}'
    mail(to: @mypost.user.email, subject: "New Post")
  end
end
